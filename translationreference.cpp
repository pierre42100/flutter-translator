#include "translationreference.h"

TranslationReference::TranslationReference()
{

}

TranslationReference::TranslationReference(const QString &phrase)
    : TranslationEntry(phrase)
{

}

QList<CodeReference> TranslationReference::references() const
{
    return mReferences;
}

void TranslationReference::addReference(const CodeReference &reference)
{
    mReferences.append(reference);
}

void TranslationReference::addReferences(const QList<CodeReference> &references)
{
    mReferences += references;
}

void TranslationReference::setReferences(const QList<CodeReference> &references)
{
    mReferences = references;
}

bool TranslationReference::operator==(const TranslationEntry &other)
{
    return TranslationEntry::operator ==(other);
}
