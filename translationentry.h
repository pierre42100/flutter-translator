#ifndef TRANSLATIONENTRY_H
#define TRANSLATIONENTRY_H

#include <QObject>

/**
 * Single translation entry
 *
 * @author Pierre HUBERT
 */

class TranslationEntry
{
public:
    TranslationEntry();
    TranslationEntry(const QString &phrase);

    bool isValid() const;

    QString phrase() const;
    void setPhrase(const QString &phrase);

    bool operator==(const TranslationEntry &other);


private:
    QString mPhrase;

};

bool operator<(const TranslationEntry &one, const TranslationEntry &other);

#endif // TRANSLATIONENTRY_H
