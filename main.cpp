#include <QApplication>
#include <QTimer>
#include "commandlinecontroller.h"
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Check if we have to run in command line or graphical mode
    if(QCoreApplication::arguments().length() < 2) {
        MainWindow *w = new MainWindow;
        w->show();
    }

    else {
        CommandLineController *controller = new CommandLineController();
        QTimer::singleShot(0, controller, &CommandLineController::exec);
    }

    return a.exec();
}
