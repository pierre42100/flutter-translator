/**
 * Translations references list
 *
 * @author Pierre HUBERT
 */

#pragma once

#include "translatedentrieslist.h"

#include <QList>

#include <translationreference.h>

class TranslationsReferencesList : public QList<TranslationReference>
{
public:
    TranslationsReferencesList();

    /**
     * Find a reference specified by its phrase
     *
     * @param phrase The phrase of the reference to find
     * @return Matching reference or null in case of failure
     */
    const TranslationReference *find(const QString &phrase) const;

    /**
     * Returns the list of untranslated keys of the application
     *
     * @param list The list of translations
     * @return The list of untranslated entries
     */
    const TranslationsReferencesList getUntranslated(const TranslatedEntriesList &translations);
    const TranslationsReferencesList getUntranslated(const QList<TranslatedEntriesList> &translations);
};
