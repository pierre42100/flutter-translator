/**
 * Single code reference
 *
 * @author Pierre HUBERT
 */

#include <QString>

#pragma once


class CodeReference
{
public:
    CodeReference();
    CodeReference(const QString &file, int begin, int end);

    QString file() const;
    void setFile(const QString &file);


    int begin() const;
    void setBegin(int begin);

    int end() const;
    void setEnd(int end);

private:
    QString mFile;
    int mBegin;
    int mEnd;
};
