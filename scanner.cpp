#include "scanner.h"

#include <QDir>
#include <QRegExp>

Scanner::Scanner()
{

}

TranslationsReferencesList Scanner::scan(const QString &path)
{
    QString dir(path  +
                (path.endsWith(QDir::separator()) ? "" : QString(QDir::separator())) +
                "lib" + QDir::separator());

    // Make recursive scan of the directory
    return recursiveScan(QDir(dir));
}

TranslationsReferencesList Scanner::recursiveScan(const QDir &dir)
{
    TranslationsReferencesList list;


    // Scan the directory
    QStringList ls = dir.entryList();

    for(QString file : ls) {
        if(file == "." || file == "..") continue;

        QFileInfo info(dir, file);
        QList<TranslationReference> entries;

        // If the entry if a directory, recurse the scan
        if(info.isDir())
            entries = recursiveScan(QDir(dir.absolutePath() +"/"+ file));
        // Otherwise scan the file
        else
            entries = scanFile(info.absoluteFilePath());

        for(TranslationReference entry : entries)
            if(!list.contains(entry))
                list.append(entry);
            else
                list[list.indexOf(entry)].addReferences(entry.references());
    }

    return list;

}

TranslationsReferencesList Scanner::scanFile(const QString &path)
{
    TranslationsReferencesList list;
    QFile file(path);

    // Check whether this file is a dart file or not
    if(!file.fileName().endsWith(".dart"))
        return list; // Ignore this file

    // Open the file to scan it
    if(!file.open(QFile::ReadOnly)){
        qFatal("Could not open a file ! (%s)", file.fileName().toStdString().c_str());
        return list;
    }

    QString content = file.readAll();

    file.close();

    // Now we can analyse the code
    // Search all the references to the tr function
    int pos = 0;
    while(content.indexOf("tr(", pos) != -1){

        int loc = content.indexOf("tr(", pos);
        pos = loc + 1;
        if(loc > 0 && content.at(loc - 1).isLetter())
            continue;
        int begin = -1;
        int end = -1;

        int search_pos = loc + 3;

        while(content.length() > search_pos){

            // Search for opening and closing brace
            if(content[search_pos] == '"' && begin == -1)
                begin = search_pos + 1;

            else if(content[search_pos] == '"' && content[search_pos - 1] != '\\'){
                    end = search_pos;
                    break;
            }

            else if(begin == -1 && (content[search_pos] == ','
                                    || content[search_pos] == ')'
                                    || content[search_pos] == '+'
                                    || content[search_pos] == '?'
                                    || content[search_pos] == ':'
                                    || content[search_pos] == ';'))
                break;

            search_pos++;
        }


        // Check if the string was not found
        if(begin == -1 || end == -1)
            continue;


        // Extract the string
        QString string = content.mid(begin, end - begin);
        TranslationReference ref(string);
        ref.addReference(CodeReference(path, begin, end));
        list.append(ref);
    }

    return list;
}
