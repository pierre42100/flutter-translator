#include "translatedentrieslist.h"

TranslatedEntriesList::TranslatedEntriesList(const QString &lang) :
    QList(), mLang(lang)
{

}

QString TranslatedEntriesList::lang() const
{
    return mLang;
}

void TranslatedEntriesList::setLang(const QString &lang)
{
    mLang = lang;
}

TranslatedEntry TranslatedEntriesList::getEntry(const TranslationEntry &entry) const
{

    int index = findEntryIndex(entry);
    if(index >= 0)
            return at(index);

    return TranslatedEntry(entry.phrase(), "", lang());
}

int TranslatedEntriesList::findEntryIndex(const TranslationEntry &entry) const
{
    for(int i = 0; i < count(); i++)
        if(entry.phrase() == at(i).phrase())
            return i;

    return -1;
}

void TranslatedEntriesList::insertOrUpdate(const TranslatedEntry &entry)
{
    int index = findEntryIndex(entry);

    if(index >= 0)
        replace(index, entry);
    else
        append(entry);
}
