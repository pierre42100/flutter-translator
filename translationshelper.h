/**
 * Translations helper
 *
 * Helps to manage translations in different languages
 *
 * @author Pierre HUBERT
 */

#pragma once

#include "translatedentrieslist.h"

#include <QObject>

class TranslationsHelper
{
public:
    TranslationsHelper();

    /**
     * Check out wheter a given application path seems to
     * be valid or not
     *
     * @param appPath The application path to check
     * @return TRUE if app paht seems to be valid / FALSE else
     */
    bool checkAppPathValidity(const QString &appPath);


    /**
     * Checkout whether a given translation exists or not
     * @param appPath Application path
     * @param name The name of the translation to check
     * @return TRUE if the translation exists / FALSE else
     */
    bool existsTranslation(const QString &appPath, const QString &name);

    /**
     * Create a  new translation file
     *
     * @param appPath The path to the application
     * @param name The codename of the new translation
     * @return TRUE in case of success or FALSE in case of failure
     */
    bool createTranslation(const QString &appPath, const QString &name);


    /**
     * Get the list of available translations for an application
     *
     * @param appPath Path to the target application
     * @return The list of available translations
     */
    QStringList getTranslationsList(const QString &appPath);

    /**
     * Get the available translations of a given language
     *
     * @param appPath Path to the application
     * @param name The name of the language to get
     * @return The list of translations for the language
     */
    TranslatedEntriesList getTranslations(const QString &appPath, const QString &name);

    /**
     * Save a new list of translations for a given language
     *
     * @param appPath Path to the application
     * @param list The list of translations to save
     * @return TRUE in case of success / FALSE else
     */
    bool saveTranslations(const QString &appPath, TranslatedEntriesList &list);

private:
    /**
     * Get the path to a translation file
     *
     * @param appPath The path to the application
     * @param name Translation codename
     * @return TRUE in case of success / FALSE else
     */
    QString getTranslationFilePath(const QString &appPath, const QString name);

    /**
     * Get the path to the directory that contains translation files
     *
     * @param appPath The path to the application
     * @return TRUE in case of success / FALSE else
     */
    QString getTranslationDirectoryPath(const QString &appPath);
};
