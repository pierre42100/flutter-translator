#include "translationentry.h"

TranslationEntry::TranslationEntry()
{

}

TranslationEntry::TranslationEntry(const QString &phrase) : mPhrase(phrase)
{

}

bool TranslationEntry::isValid() const
{
    return mPhrase.length() > 0;
}

QString TranslationEntry::phrase() const
{
    return mPhrase;
}

void TranslationEntry::setPhrase(const QString &phrase)
{
    mPhrase = phrase;
}

bool TranslationEntry::operator==(const TranslationEntry &other)
{
    return mPhrase == other.phrase();
}

bool operator<(const TranslationEntry &one, const TranslationEntry &other)
{
    return one.phrase() < other.phrase();
}
