/**
 * Translations table
 *
 * This widget display the list of currently available translations
 *
 * @author Pierre HUBERT
 */

#ifndef TRANSLATIONSTABLE_H
#define TRANSLATIONSTABLE_H

#include "translatedentrieslist.h"
#include "translationsreferenceslist.h"

#include <QTableWidget>

class TranslationsTable : public QTableWidget
{
    Q_OBJECT
public:
    explicit TranslationsTable(QWidget *parent = 0);

    TranslationsReferencesList entries() const;
    void setEntries(const TranslationsReferencesList &entries);

    QList<TranslatedEntriesList> translations() const;
    void setTranslations(const QList<TranslatedEntriesList> &translations);

    /**
     * Get / set the translated entry at a specific place
     */
    TranslatedEntry getEntryAt(int row, int col);

    void setEntryAt(int col, TranslatedEntry entry);

    bool getShowOnlyUntranslatedKeys() const;
    void setShowOnlyUntranslatedKeys(bool showOnlyUntranslatedKeys);

public slots:

    /**
     * Rebuild the entire array
     */
    void rebuild();

signals:

    /**
     * Signal emitted to notify that some data have been
     * updated in the tables that might require to be
     * saved now or later
     */
    void dataUpdated();

private slots:
    void updatedCellContent(int row, int column);
    void doubleClickedCell(int row, int column);

private:

    //Private members
    TranslationsReferencesList mEntries;
    QList<TranslatedEntriesList> mTranslations;
    bool mShowOnlyUntranslatedKeys = false;
};

#endif // TRANSLATIONSTABLE_H
