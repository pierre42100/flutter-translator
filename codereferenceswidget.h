/**
 * Code references widget
 *
 * This widget allows to jump to some lines of code
 *
 * @author Pierre HUBERT
 */

#pragma once

#include "codereference.h"

#include <QWidget>

namespace Ui {
class CodeReferencesWidget;
}

class CodeReferencesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CodeReferencesWidget(QWidget *parent, const QList<CodeReference> &references);
    ~CodeReferencesWidget();

    void addReference(const CodeReference &ref);

private:
    Ui::CodeReferencesWidget *ui;
};
