#include "codereferenceswidget.h"
#include "ui_codereferenceswidget.h"

#include <QFile>
#include <QFileInfo>
#include <QTextEdit>

CodeReferencesWidget::CodeReferencesWidget(QWidget *parent,
                                           const QList<CodeReference> &references) :
    QWidget(parent),
    ui(new Ui::CodeReferencesWidget)
{
    ui->setupUi(this);

    // Process the list of code references
    for(CodeReference ref : references)
        addReference(ref);
}

CodeReferencesWidget::~CodeReferencesWidget()
{
    delete ui;
}

void CodeReferencesWidget::addReference(const CodeReference &ref)
{
    QFile file(ref.file());

    if(!file.open(QIODevice::ReadOnly))
        qFatal("Could not open the file for reading!");

    QString content = file.readAll();
    file.close();

    //Highlight target text
    content.insert(ref.end(), "</b>");
    content.insert(ref.begin(), "<b>");
    content.replace("\n", "<br/>");
    content.replace("\t", "     ");
    content.replace(" ", "&nbsp;");


    QTextEdit *codeWidget = new QTextEdit();
    codeWidget->setHtml(content);
    codeWidget->setReadOnly(true);

    ui->referencesWidget->addTab(codeWidget, QFileInfo(ref.file()).fileName());
}
