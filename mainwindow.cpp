#include <QMessageBox>
#include <QInputDialog>
#include <QFileDialog>

#include "mainwindow.h"
#include "scanner.h"
#include "translationshelper.h"
#include "translatedentrieslist.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->translationsTable, &TranslationsTable::dataUpdated,
            this, &MainWindow::dataUpdated);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::chooseApplicationDirectory()
{
    QString dir =
            QFileDialog::getExistingDirectory(this, tr("Pick application directory"));

    if(dir.isEmpty())
        return;

    ui->projectLocation->setText(dir);
    fullRescan();
}

QString MainWindow::currentAppLocation()
{
    return ui->projectLocation->text();
}

void MainWindow::refreshLangs()
{
    QStringList translations =
            mTranslationsHelper->getTranslationsList(currentAppLocation());

    QList<TranslatedEntriesList> list;
    for(QString entry : translations)
        list.append(
            mTranslationsHelper->getTranslations(
                currentAppLocation(), entry
            )
        );

    ui->translationsTable->setTranslations(list);
}

void MainWindow::save()
{
    for(TranslatedEntriesList list : ui->translationsTable->translations())
        if(!mTranslationsHelper->saveTranslations(
                    currentAppLocation(), list))
            QMessageBox::warning(this,
                                 tr("Error"),
                                 tr("Could not save translations for %1 !").arg(list.lang()));

    setUnsavedState(false);
}

void MainWindow::setUnsavedState(bool enable)
{
    if(enable && !(windowTitle().left(2) == "* "))
        setWindowTitle("* " + windowTitle());

    else if(!enable && windowTitle().left(2) == "* ")
        setWindowTitle(windowTitle().remove(0, 2));
}

void MainWindow::fullRescan()
{
    on_scanButton_clicked();
    refreshLangs();
}

void MainWindow::dataUpdated()
{
    setUnsavedState(true);
}

void MainWindow::on_actionExit_triggered()
{
    QCoreApplication::exit();
}

void MainWindow::on_scanButton_clicked()
{
    //QMessageBox::information(this, tr("Begin scan"), tr("The scan might take a while, please be patient..."));

    ui->translationsTable->setEntries(Scanner().scan(currentAppLocation()));

}

void MainWindow::on_actionCreate_new_translation_triggered()
{

    QString translation = QInputDialog::getText(
                this,
                tr("Create a new translation"),
                tr("Please specify the code name of the new translation (eg: fr)"));

    if(translation.isEmpty() || translation.isNull())
        return;

    if(translation.contains(" ") || translation.contains(".")) {
        QMessageBox::warning(this, tr("Error"), tr("Invalid translation!"));
        return;
    }

    // Create the new translation
    if(!mTranslationsHelper->createTranslation(currentAppLocation(), translation)){
        QMessageBox::warning(this, tr("Error"), tr("Could not create the translation file!"));
        return;
    }

    refreshLangs();
}

void MainWindow::on_actionRefreshLangs_triggered()
{
    refreshLangs();
}

void MainWindow::on_refreshLangs_clicked()
{
    refreshLangs();
}

void MainWindow::on_actionSave_triggered()
{
    save();
}

void MainWindow::on_pushButton_clicked()
{
    fullRescan();
}

void MainWindow::on_checkBox_toggled(bool checked)
{
    ui->translationsTable->setShowOnlyUntranslatedKeys(checked);
}

void MainWindow::on_openDirectoryButton_clicked()
{
    chooseApplicationDirectory();
}
