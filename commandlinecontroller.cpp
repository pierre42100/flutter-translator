#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QTextStream>

#include "commandlinecontroller.h"
#include "scanner.h"
#include "translationshelper.h"

CommandLineController::CommandLineController(QObject *parent) : QObject(parent)
{

}

void CommandLineController::showHelp()
{
    msg("Flutter translator. A tool to easily translate Flutter applications.\n"
        "(c) Pierre HUBERT 2019 - Licensed under the MIT License\n"
        "Command Line usage :\n"
        "\thelp : Show this help\n"
        "\tfix_missing [app_path] [lang] : Fix missing translations for a project\n");
}

void CommandLineController::fixMissingTranslations()
{
    QTextStream qtin(stdin);

    // Check for requested arguments
    if(QCoreApplication::arguments().length() < 4)
        fatalError("Missing arguments !");

    // Get application path & language
    QString path = QCoreApplication::arguments()[2];
    QString name = QCoreApplication::arguments()[3];

    if(!TranslationsHelper().checkAppPathValidity(path))
        fatalError("Specified application directory does not seems to be valid!");

    if(!TranslationsHelper().existsTranslation(path, name))
        fatalError("Specified translation does not exists!");

    // Scan project
    msg("Scanning project");
    TranslationsReferencesList list = Scanner().scan(path);

    // Get translations
    TranslatedEntriesList translations =
            TranslationsHelper().getTranslations(path, name);

    // Ignore translated entries
    list = list.getUntranslated(translations);

    for(TranslationReference ref : list) {

        // Ask for translation
        printf("%s : ", ref.phrase().toStdString().c_str());

        QString entry = qtin.readLine();

        if(entry.length() == 0 || entry == " ")
            continue;

        // Apply new translation
        TranslatedEntry trentry = translations.getEntry(ref);
        trentry.setTranslation(entry);
        translations.insertOrUpdate(trentry);

        // Save translations
        if(!TranslationsHelper().saveTranslations(path, translations))
            fatalError("Could not save translation !");

    }

    msg("Done.");
}

void CommandLineController::exec()
{
    //Process command line arguments
    if(QCoreApplication::arguments().count() < 2)
        fatalError("No argument specified!");

    const QString command = QCoreApplication::arguments().at(1);

    if(command == "help")
        showHelp();
    else if(command == "fix_missing")
        fixMissingTranslations();
    else
        fatalError(QString("Option %1 not recognized!").arg(command));

    QCoreApplication::quit();
}

void CommandLineController::msg(const QString &msg) const
{
    qDebug("%s\n", msg.toStdString().c_str());
}

void CommandLineController::fatalError(const QString &msg) const
{
    qFatal("Fatal: %s", msg.toStdString().c_str());
}
