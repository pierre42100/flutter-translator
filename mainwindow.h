#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class TranslationsHelper;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /**
     * Get and set application directory
     */
    void chooseApplicationDirectory();
    QString currentAppLocation();

    /**
     * Refresh information about the langs
     * of the project
     */
    void refreshLangs();


    /**
     * Save result
     */
    void save();

    /**
     * Update the UI to mark requires update
     *
     * @param enable Enable unsaved mode or not
     */
    void setUnsavedState(bool enable);


public slots:
    void fullRescan();


private slots:

    void dataUpdated();

    void on_actionExit_triggered();

    void on_scanButton_clicked();

    void on_actionCreate_new_translation_triggered();

    void on_actionRefreshLangs_triggered();

    void on_refreshLangs_clicked();

    void on_actionSave_triggered();

    void on_pushButton_clicked();

    void on_checkBox_toggled(bool checked);

    void on_openDirectoryButton_clicked();

private:
    Ui::MainWindow *ui;
    TranslationsHelper *mTranslationsHelper;
};

#endif // MAINWINDOW_H
