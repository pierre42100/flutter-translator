#include "translatedentry.h"

TranslatedEntry::TranslatedEntry()
{

}

TranslatedEntry::TranslatedEntry(const QString &phrase,
                                 const QString &translation,
                                 const QString &lang)
    : TranslationEntry(phrase),
        mTranslation(translation),
        mLang(lang)
{

}

QString TranslatedEntry::translation() const
{
    return mTranslation;
}

void TranslatedEntry::setTranslation(const QString &translation)
{
    mTranslation = translation;
}

QString TranslatedEntry::lang() const
{
    return mLang;
}

void TranslatedEntry::setLang(const QString &lang)
{
    mLang = lang;
}
