/**
 * Command line controller
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QObject>

class CommandLineController : public QObject
{
    Q_OBJECT
public:
    explicit CommandLineController(QObject *parent = 0);

    void showHelp();
    void fixMissingTranslations();

signals:

public slots:
    void exec();

private:
    void msg(const QString &msg) const;
    void fatalError(const QString &msg) const;
};
