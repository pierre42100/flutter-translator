#include "codereference.h"

CodeReference::CodeReference()
{

}

CodeReference::CodeReference(const QString &file, int begin, int end)
    : mFile(file), mBegin(begin), mEnd(end)
{

}

QString CodeReference::file() const
{
    return mFile;
}

void CodeReference::setFile(const QString &file)
{
    mFile = file;
}

int CodeReference::begin() const
{
    return mBegin;
}

void CodeReference::setBegin(int begin)
{
    mBegin = begin;
}

int CodeReference::end() const
{
    return mEnd;
}

void CodeReference::setEnd(int end)
{
    mEnd = end;
}
