#-------------------------------------------------
#
# Project created by QtCreator 2019-05-03T08:19:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = flutter_translator
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    translationentry.cpp \
    scanner.cpp \
    translationstable.cpp \
    translationshelper.cpp \
    translatedentry.cpp \
    translatedentrieslist.cpp \
    codereference.cpp \
    translationreference.cpp \
    translationsreferenceslist.cpp \
    codereferenceswidget.cpp \
    commandlinecontroller.cpp

HEADERS  += mainwindow.h \
    translationentry.h \
    scanner.h \
    translationstable.h \
    translationshelper.h \
    translatedentry.h \
    translatedentrieslist.h \
    codereference.h \
    translationreference.h \
    translationsreferenceslist.h \
    codereferenceswidget.h \
    commandlinecontroller.h

FORMS    += mainwindow.ui \
    codereferenceswidget.ui
