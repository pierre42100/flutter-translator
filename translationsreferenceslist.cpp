#include "translationsreferenceslist.h"

TranslationsReferencesList::TranslationsReferencesList()
{

}

TranslationReference const * TranslationsReferencesList::find(const QString &phrase) const
{

    for(int i = 0; i < length(); i++)
        if(at(i).phrase() == phrase)
            return &at(i);

    return nullptr;
}

const TranslationsReferencesList TranslationsReferencesList::getUntranslated(const TranslatedEntriesList &translations)
{
    QList<TranslatedEntriesList> list;
    list << translations;
    return getUntranslated(list);
}

const TranslationsReferencesList TranslationsReferencesList::getUntranslated(const QList<TranslatedEntriesList> &translations)
{
    TranslationsReferencesList list;
    for(TranslationReference ref : *this){

        // Check if the key has been translated in all languages or not
        bool toAdd = false;

        for(TranslatedEntriesList translation : translations)
            if(translation.getEntry(ref).translation().isEmpty()) {
                toAdd = true;
                break;
            }

        if(toAdd)
            list.append(ref);
    }

    return list;
}
