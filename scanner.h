/**
 * Project scanner
 *
 * Extract all translatable references from a Flutter project
 *
 * @author Pierre HUBERT
 */

#pragma once


#include "translationsreferenceslist.h"

class QDir;
class QFile;

class Scanner
{
public:
    Scanner();

    /**
     * @brief scan Scan a directory for translation references
     * @param path Path to the Flutter source code
     * @return Extracted translation references
     */
    TranslationsReferencesList scan(const QString &path);

private:

    /**
     * @brief recursiveScan Scan a directory in a recursive way
     * @param dir Directory to scan
     * @return The list of translations found in the directory
     */
    TranslationsReferencesList recursiveScan(const QDir &dir);

    /**
     * Scan a file to determine all the translation entries it contains
     * @param file The file to scan
     * @return The list of extracted translation entries
     */
    TranslationsReferencesList scanFile(const QString &path);
};
