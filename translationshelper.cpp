#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>

#include "translationshelper.h"

TranslationsHelper::TranslationsHelper()
{

}

bool TranslationsHelper::checkAppPathValidity(const QString &appPath)
{
    QDir path(appPath);

    if(!path.exists()) {
        qDebug("Specified application path does not exists.");
        return false;
    }

    if(!path.entryList().contains("lib")){
        qDebug("Specified application path does not contains a directory lib");
        return false;
    }

    return true;
}

bool TranslationsHelper::existsTranslation(const QString &appPath, const QString &name)
{
    return QFile(getTranslationFilePath(appPath, name))
            .exists();
}

bool TranslationsHelper::createTranslation(
        const QString &appPath, const QString &name)
{
    // Get translation path
    QString path = getTranslationFilePath(appPath, name);

    if(existsTranslation(appPath, name)) return false;

    QFile file(path);

    // Before trying to create the file we have to make
    // sure the parent directory exists
    QDir parent = QFileInfo(file).dir();

    if(!parent.mkpath("."))
        return false;

    // Try to create the file
    if(!file.open(QFile::WriteOnly))
        return false;

    if(!file.write("{}"))
        return false;

    if(!file.flush())
        return false;

    file.close();

    return true;

}

QStringList TranslationsHelper::getTranslationsList(const QString &appPath)
{
    QDir container(getTranslationDirectoryPath(appPath));

    if(!container.exists())
        return QStringList();

    QStringList list;
    for(QString file : container.entryList())
        if(file.endsWith(".json"))
            list << file.replace(".json", "");

    return list;
}

TranslatedEntriesList TranslationsHelper::getTranslations(const QString &appPath, const QString &name)
{
    QFile file(getTranslationFilePath(appPath, name));

    if(!file.exists())
        qFatal("Requested translations for a file that does not exists!");

    // Read file content
    if(!file.open(QFile::ReadOnly))
        qFatal("Could not open a translations file !");
    QByteArray content = file.readAll();
    file.close();

    // Parse JSON
    QJsonObject doc = QJsonDocument::fromJson(content).object();

    TranslatedEntriesList entries(name);
    for(QJsonObject::iterator ite = doc.begin(); ite != doc.end(); ite++)
        entries.append(TranslatedEntry(ite.key(), ite.value().toString(), name));

    return entries;

}

bool TranslationsHelper::saveTranslations(const QString &appPath, TranslatedEntriesList &list)
{
    QFile file(getTranslationFilePath(appPath, list.lang()));

    if(!file.exists())
        qFatal("Requested translations for a file that does not exists!");

    if(!file.open(QIODevice::WriteOnly))
        qFatal("Could not write to a translation file!");

    // Sort entries
    std::sort(list.begin(), list.end());

    // Convert entries to JSON
    QJsonObject object;
    for(TranslatedEntry entry : list)
        if(!entry.translation().isEmpty())
            object.insert(entry.phrase(), entry.translation());

    // Save entries
    file.write(QJsonDocument(object).toJson(QJsonDocument::Indented));
    bool result = file.flush();
    file.close();

    return result;
}

QString TranslationsHelper::getTranslationFilePath(const QString &appPath, const QString name)
{

    return  getTranslationDirectoryPath(appPath) + name + ".json";
}

QString TranslationsHelper::getTranslationDirectoryPath(const QString &appPath)
{
    return appPath + QDir::separator() +
            "assets" + QDir::separator() +
            "langs" + QDir::separator();
}
