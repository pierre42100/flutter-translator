#include "translationstable.h"
#include "codereferenceswidget.h"

TranslationsTable::TranslationsTable(QWidget *parent) : QTableWidget(parent)
{
    connect(this, &TranslationsTable::cellChanged,
            this, &TranslationsTable::updatedCellContent);
    connect(this, &TranslationsTable::cellDoubleClicked,
            this, &TranslationsTable::doubleClickedCell);
}


TranslationsReferencesList TranslationsTable::entries() const
{
    return mEntries;
}


void TranslationsTable::setEntries(const TranslationsReferencesList &entries)
{
    mEntries = entries;    rebuild();
}

void TranslationsTable::rebuild()
{
    TranslationsReferencesList list;
    if(!mShowOnlyUntranslatedKeys)
        list = entries();
    else
        list = entries().getUntranslated(mTranslations);

    setRowCount(list.count());
    setColumnCount(1 + mTranslations.count());

    setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Initial language")));

    for(int i = 0; i < mTranslations.count(); i++)
        setHorizontalHeaderItem(i + 1, new QTableWidgetItem(mTranslations[i].lang()));

    //Process the list of entries
    for(int i = 0; i < list.count(); i++) {
        setItem(i, 0, new QTableWidgetItem(list[i].phrase()));
       item(i, 0)->setFlags(Qt::ItemIsEnabled);

        //Process the list of translations
        for(int j = 0; j < mTranslations.count(); j++){
            TranslatedEntry entry = mTranslations[j].getEntry(list[i]);

            if(entry.isValid())
                setItem(i, 1 + j, new QTableWidgetItem(entry.translation()));
        }

    }
}

void TranslationsTable::updatedCellContent(int row, int column)
{
    if(column == 0) return;

    //Get the content of the cell
    QString newTranslation = item(row, column)->text();
    TranslatedEntry entry = getEntryAt(row, column);
    entry.setTranslation(newTranslation);
    setEntryAt(column, entry);

    emit dataUpdated();
}

void TranslationsTable::doubleClickedCell(int row, int column)
{
    if(column > 0) return;

    //We need to show the corresponding code reference
    const TranslationReference *ref = entries().find(item(row, column)->text());

    if(ref == nullptr)
        qFatal("Translation reference must not be null!");

    //We have to show the code references
    (new CodeReferencesWidget(nullptr, ref->references()))->show();

}

bool TranslationsTable::getShowOnlyUntranslatedKeys() const
{
    return mShowOnlyUntranslatedKeys;
}

void TranslationsTable::setShowOnlyUntranslatedKeys(bool showOnlyUntranslatedKeys)
{
    mShowOnlyUntranslatedKeys = showOnlyUntranslatedKeys;
    rebuild();
}

TranslatedEntry TranslationsTable::getEntryAt(int row, int col)
{
    return mTranslations[col-1].getEntry(*mEntries.find(item(row, 0)->text()));
}

void TranslationsTable::setEntryAt(int col, TranslatedEntry entry)
{
    int index = mTranslations[col-1].findEntryIndex(entry);
    if(index < 0)
        mTranslations[col-1].append(entry);
    else
        mTranslations[col-1].replace(index, entry);
}

QList<TranslatedEntriesList> TranslationsTable::translations() const
{
    return mTranslations;
}

void TranslationsTable::setTranslations(const QList<TranslatedEntriesList> &translations)
{
    mTranslations = translations;
    rebuild();
}
