/**
 * Translated entries list
 *
 * This class provides methods to quickly get and set a translation
 * for a specified language
 *
 * @author Pierre HUBERT
 */

#include "translatedentry.h"

#include <QList>

#pragma once

class TranslatedEntriesList : public QList<TranslatedEntry>
{
public:
    TranslatedEntriesList(const QString &lang);

    QString lang() const;
    void setLang(const QString &lang);

    /**
     * Find the entry associated with a translation entry
     *
     * Returns null if none null
     *
     * @param entry The entry to search
     * @return TRUE if found / FALSE else
     */
    TranslatedEntry getEntry(const TranslationEntry &entry) const;

    /**
     * Find an entry with a given index
     *
     * @param entry Target entry information
     * @return Matching entry
     */
    int findEntryIndex(const TranslationEntry &entry) const;

    /**
     * Insert or update a translation entry
     *
     * @param entry The entry to insert / update
     */
    void insertOrUpdate(const TranslatedEntry &entry);

private:
    QString mLang;
};
