/**
 * Translation reference
 *
 * @author Pierre HUBERT
 */
#pragma once

#include "codereference.h"
#include "translationentry.h"

#include <QObject>
#include <QList>

class TranslationReference : public TranslationEntry
{
public:
    TranslationReference();
    TranslationReference(const QString &phrase);

    QList<CodeReference> references() const;
    void addReference(const CodeReference &reference);
    void addReferences(const QList<CodeReference> &references);
    void setReferences(const QList<CodeReference> &references);

    bool operator==(const TranslationEntry &other);

private:
    QList<CodeReference> mReferences;
};
