/**
 * Single entry translated into another language
 *
 * @author Pierre HUBERT
 */

#include "translationentry.h"

#pragma once


class TranslatedEntry : public TranslationEntry
{
public:
    TranslatedEntry();
    TranslatedEntry(const QString &phrase,
                    const QString &translation,
                    const QString &lang);

    QString translation() const;
    void setTranslation(const QString &translation);

    QString lang() const;
    void setLang(const QString &lang);

private:
    QString mTranslation;
    QString mLang;
};
